package wordFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class WordFilter {

	List<List<String>> censorWords = new ArrayList<>();
//	List<String> ngWords = new ArrayList<>();
	Map<String, Map<String, Integer>> userMap = new HashMap<>();

	String replaceWord = "<censored>";

	public WordFilter(String word) {
		addWord(word);
		setUser("no_name:");
	}

	public int getUserTotalNgWord(String user) {
		String db_user = user + ":";
		if (userMap.get(db_user) == null) {
			return 0;
		}
		return userMap.get(db_user).values().stream().mapToInt(x -> x).sum();
	}
	
	public int getTotalNgWord(String word) {
		int sum = 0;
		for (Map<String, Integer> words: userMap.values()) {
			sum += words.entrySet().stream()
					.filter(e -> e.getKey() == word)
					.mapToInt(e -> e.getValue()).sum();
		}
		return sum;
	}

	public void addUserWords(String user, String word) {
		if (userMap.get(user) == null) {
			this.setUser(user);
		}
		if (userMap.get(user).get(word) == null) {
			userMap.get(user).put(word, 1);
		}
		else {
			int count = userMap.get(user).get(word);
			userMap.get(user).put(word, count+1);
		}
	}

	public void setUser(String user) {
		userMap.put(user, new HashMap<String, Integer>());
	}

	public void setReplaceWord(String word) {
		replaceWord = word;
	}

	public void addWord(String...words) {
		List<String> wordList = new ArrayList<>();

		for (String word: words) {
			wordList.add(word);
		}
		censorWords.add(wordList);
	}

	public void removeWord(String...words) {
		List<String> wordList = new ArrayList<>();

		for (String word: words) {
			wordList.add(word);
		}

		censorWords.remove(wordList);
	}
	
	public String[] splitUser(String message) {
       Pattern pattern = Pattern.compile("^[a-zA-Z0-9_-]+:");
		String[] split = pattern.split(message);
		
		if (split.length == 2) {
			Matcher matcher = pattern.matcher(message);
			matcher.find();
			String[] msg = {matcher.group(0), split[1]};
			return msg;
		}
		else {
			String[] msg = {"no_name:", split[0]};
			return msg;
		}
	}

	public boolean detect(String message) {

		String[] split = splitUser(message);
		List<Integer> detectIndexes = detectIndex(split[1]);
		if (detectIndexes.size() != 0) {
			for (int detectIndex: detectIndexes) {
				for (String word: censorWords.get(detectIndex)) {
					this.addUserWords(split[0], word);
				}
			}
			return true;
		}
		else {
			return false;
		}
	}

	public List<Integer> detectIndex(String message) {
		int last;
		List<String> words;
		List<Integer> indexes = new LinkedList<>();

		for (int censorIndex = 0; censorIndex < censorWords.size(); ++ censorIndex) {
			words = censorWords.get(censorIndex);
			last = words.size()-1;
			for (int index = 0; index < words.size(); ++ index) {
				if (message.indexOf(words.get(index)) == -1) {
					break;
				}
				if (last == index) {
					indexes.add(censorIndex);
				}
			}
		}
		return indexes;
	}

	public String censor(String message) {
		String[] split = splitUser(message);
		List<Integer> detectIndexes = detectIndex(split[1]);

		if (detectIndexes.size() >= 0) {
			String replaceString = split[1];

			for (int detectIndex: detectIndexes) {
				for (String word: censorWords.get(detectIndex)) {
					replaceString = replaceString.replace(word, replaceWord);
					this.addUserWords(split[0], word);
				}
			}
			return split[0] + replaceString;
		}

		return message;
	}
}
