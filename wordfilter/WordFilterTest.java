package wordFilter;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

class WordFilterTest {

	@Test
	void detectTest() {
		WordFilter filter = new WordFilter("Arsenal");

		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertFalse(filter.detect("t_wada: 昨日の ManU vs Lioverpool そうでもなかった!"));

	}

	@Test
	void censorTest() {
		WordFilter filter = new WordFilter("Arsenal");

		assertEquals(filter.censor("t_wada: 昨日の Arsenal vs Chelsea 熱かった."),
				"t_wada: 昨日の <censored> vs Chelsea 熱かった.");
		assertEquals(filter.censor("t_wada: 昨日の ManU vs Lioverpool そうでもなかった!"),
				"t_wada: 昨日の ManU vs Lioverpool そうでもなかった!");
	}

	@Test
	void addTest() {
		WordFilter filter = new WordFilter("Arsenal");

		filter.addWord("Chelsea");

		assertEquals(filter.censor("t_wada: 昨日の Arsenal vs Chelsea 熱かった."),
				"t_wada: 昨日の <censored> vs <censored> 熱かった.");
	}

	@Test
	void removeTest() {
		WordFilter filter = new WordFilter("Arsenal");

		filter.removeWord("Arsenal");
		assertEquals(filter.censor("t_wada: 昨日の Arsenal vs Chelsea 熱かった."),
				"t_wada: 昨日の Arsenal vs Chelsea 熱かった.");
	}

	@Test
	void replaceWordTest() {
		WordFilter filter = new WordFilter("Arsenal");

		filter.setReplaceWord("<>");

		assertEquals(filter.censor("t_wada: 昨日の Arsenal vs Chelsea 熱かった."),
				"t_wada: 昨日の <> vs Chelsea 熱かった.");
	}

	@Test
	void ngUserTest() {
		WordFilter filter = new WordFilter("Arsenal");
		
		filter.addWord("t_wada");
		
		assertEquals(filter.censor("t_wada: 昨日の Arsenal vs Chelsea 熱かった."),
				"t_wada: 昨日の <censored> vs Chelsea 熱かった.");
		assertEquals(filter.censor("t_wada: 昨日の Arsenal vs Chelsea 熱かった(by t_wada)."),
				"t_wada: 昨日の <censored> vs Chelsea 熱かった(by <censored>).");
	}
	
	@Test
	void ngUserCountTest() {
		WordFilter filter = new WordFilter("Arsenal");

		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertFalse(filter.detect("t_wada: 昨日の ManU vs Lioverpool そうでもなかった!"));
		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));

		assertEquals(filter.getUserTotalNgWord("t_wada"), 4);
	}
	
	@Test
	void ngCountTest() {
		WordFilter filter = new WordFilter("Arsenal");

		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertFalse(filter.detect("t_wada: 昨日の ManU vs Lioverpool そうでもなかった!"));
		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));
		assertTrue(filter.detect("t_wada: 昨日の Arsenal vs Chelsea 熱かった!"));

		assertEquals(filter.getTotalNgWord("Arsenal"), 4);
	}

}
